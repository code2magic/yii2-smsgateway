<?php
return [
    'container' => [
        'definitions' => [
            \code2magic\smsGateway\SmsGateway::class => [
                'transport' => \code2magic\smsGateway\transports\DummyTransport::class,
            ],
        ],
    ],
];
