Yii2 SMS gateway
================

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run
```
php composer.phar require code2magic/yii2-smsgateway --prefer-dist
```
or add
```
"code2magic/yii2-smsgateway": "*"
```

to the `require` section of your `composer.json` file.

## Configuration

This extension is supposed to be used with [composer-config-plugin].

Else look [config/common.php] for cofiguration example.

[composer-config-plugin]:   https://github.com/hiqdev/composer-config-plugin
[config/common.php]:    config/common.php


## Usage

Use with DI
```php
$sms_gateway = \Yii::createObject(SmsGateway::class);
$ms_gateway->send($phone, $text, [])
```

or as component

```php
// config
'components' => [
    // ...
    'sms_gateway' => \code2magic\smsGateway\SmsGateway::class,
    // ...
],
// usage
\Yii::$app->sms_gateway->send($phone, $text, [])
```
