<?php
/**
 * Created by PhpStorm.
 * User: Prong
 * Date: 30.11.2018
 * Time: 15:34
 */

namespace code2magic\smsGateway\transports;


use code2magic\smsGateway\SmsTransportInterface;
use yii\helpers\ArrayHelper;

class DummyTransport implements SmsTransportInterface
{
    /**
     * Send sms
     *
     * @param $message
     * @return mixed
     */
    public function send($message)
    {
        return true;
    }

    /**
     * Get account status
     *
     * @param array $options
     * @return mixed
     */
    public function getBalance($options = [])
    {
        return ArrayHelper::getValue($options, 'value', 0);
    }
}