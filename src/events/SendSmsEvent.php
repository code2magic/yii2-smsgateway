<?php
/**
 * Created by PhpStorm.
 * User: Prong
 * Date: 03.12.2018
 * Time: 9:47
 */

namespace code2magic\smsGateway\events;


use yii\base\Event;

class SendSmsEvent extends Event
{
    /**
     * @var \yii\mail\MessageInterface the mail message being send.
     */
    public $message;
    /**
     * @var bool if message was sent successfully.
     */
    public $isSuccessful;
    /**
     * @var bool whether to continue sending an sms. Event handlers of
     * [[\common\components\smsGateway\SmsGateway::EVENT_BEFORE_SEND]] may set this property to decide whether
     * to continue send or not.
     */
    public $isValid = true;

}
