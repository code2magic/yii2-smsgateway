<?php
/**
 * Created by PhpStorm.
 * User: Prong
 * Date: 30.11.2018
 * Time: 15:11
 */

namespace code2magic\smsGateway;


interface SmsTransportInterface
{
    /**
     * Send sms
     *
     * @param mixed  $phone Recipient phone number
     * @param string $message
     *
     * @return mixed
     */
    public function send($phone, $message);

    /**
     * Get account status
     *
     * @param array $options
     *
     * @return mixed
     */
    public function getBalance($options = []);
}