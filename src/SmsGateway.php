<?php
/**
 * Created by PhpStorm.
 * User: Prong
 * Date: 30.11.2018
 * Time: 15:07
 */

namespace code2magic\smsGateway;


use code2magic\smsGateway\events\SendSmsEvent;
use yii\base\Component;
use yii\di\Instance;
use yii\helpers\Json;

/**
 * Class SmsGateway
 * @package common\components\smsGateway
 */
class SmsGateway extends Component
{
    const EVENT_BEFORE_SEND = 'beforeSend';
    const EVENT_AFTER_SEND = 'afterSend';

    /**
     * @var bool whether to save sms messages as files under [[fileTransportPath]] instead of sending them
     * to the actual recipients. This is usually used during development for debugging purpose.
     * @see fileTransportPath
     */
    public $useFileTransport = false;
    /**
     * @var string the directory where the sms messages are saved when [[useFileTransport]] is true.
     */
    public $fileTransportPath = '@runtime/sms';
    /**
     * @var callable a PHP callback that will be called by [[send()]] when [[useFileTransport]] is true.
     * The callback should return a file name which will be used to save the sms message.
     * If not set, the file name will be generated based on the current timestamp.
     *
     * The signature of the callback is:
     *
     * ```php
     * function ($mailer, $message)
     * ```
     */
    public $fileTransportCallback;

    /** @var SmsTransportInterface */
    protected $_transport;

    /**
     * Sends the given sms message.
     * This method will log a message about the sms being sent.
     * If [[useFileTransport]] is true, it will save the sms as a file under [[fileTransportPath]].
     * Otherwise, it will call [[sendMessage()]] to send the sms to its recipient(s).
     * Child classes should implement [[sendMessage()]] with the actual sms sending logic.
     * @param string $phone
     * @param string $text
     * @param array $options
     * @return bool|mixed
     */
    public function send($phone, $text, $options = [])
    {
        $message = ['phone' => $phone, 'text' => $text, 'options' => $options];
        if (!$this->beforeSend($message)) {
            return false;
        }

        \Yii::info('Sending sms "' . $text . '" to "' . $phone . '"', __METHOD__);

        if ($this->useFileTransport) {
            $isSuccessful = $this->saveMessage($message);
        } else {
            $isSuccessful = $this->sendMessage($phone,$message);
        }
        $this->afterSend($message, $isSuccessful);

        return $isSuccessful;
    }

    /**
     * This method is invoked right before sms send.
     * You may override this method to do last-minute preparation for the message.
     * If you override this method, please make sure you call the parent implementation first.
     * @param array $message
     * @return bool whether to continue sending an sms.
     */
    public function beforeSend($message)
    {
        $event = new SendSmsEvent(['message' => $message]);
        $this->trigger(self::EVENT_BEFORE_SEND, $event);
        return $event->isValid;
    }

    /**
     * Saves the message as a file under [[fileTransportPath]].
     * @param array $message
     * @return bool whether the message is saved successfully
     */
    protected function saveMessage($message)
    {
        $path = \Yii::getAlias($this->fileTransportPath);
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        if ($this->fileTransportCallback !== null) {
            $file = $path . '/' . call_user_func($this->fileTransportCallback, $this, $message);
        } else {
            $file = $path . '/' . $this->generateMessageFileName();
        }
        file_put_contents($file, Json::encode($message));

        return true;
    }

    /**
     * @return string the file name for saving the message when [[useFileTransport]] is true.
     */
    public function generateMessageFileName()
    {
        $time = microtime(true);

        return date('Ymd-His-', $time) . sprintf('%04d', (int)(($time - (int)$time) * 10000)) . '-' . sprintf('%04d', mt_rand(0, 10000)) . '.txt';
    }

    /**
     * @param $phone
     * @param $message
     *
     * @return mixed
     */
    protected function sendMessage($phone,$message)
    {
        return $this->_transport->send($phone,$message);
    }

    /**
     * This method is invoked right after sms was send.
     * You may override this method to do some postprocessing or logging based on mail send status.
     * If you override this method, please make sure you call the parent implementation first.
     * @param array $message
     * @param bool $isSuccessful
     */
    public function afterSend($message, $isSuccessful)
    {
        $event = new SendSmsEvent(['message' => $message, 'isSuccessful' => $isSuccessful]);
        $this->trigger(self::EVENT_AFTER_SEND, $event);
    }

    /**
     * @return SmsTransportInterface
     */
    public function getTransport()
    {
        return $this->_transport;
    }

    /**
     * @param $transport
     * @throws \yii\base\InvalidConfigException
     */
    public function setTransport($transport)
    {
        $this->_transport = $transport instanceof SmsTransportInterface ? $transport : Instance::ensure($transport, SmsTransportInterface::class);
    }

}